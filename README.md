Customer satisfaction is important to me most, and getting my clients to the closing table smoothly. I am proficient in all types of mortgage financing including FHA/VA, Conventional, USDA, Jumbo, Portfolio, 1031 Exchanges, Reverse Mortgages, Construction loans, and FHA 203K loans.

Address: 1127 Auraria Parkway, #4, Denver, CO 80204

Phone: 720-524-3215
